# Reto 5

El reto consiste en construir chart en helm y manejar trafico http(s).

Instalar helm
-------------

Para instalar helm, seguir los pasos indicados de la pág: https://helm.sh/docs/intro/install/

Deploy
------

Al igual que en el reto anterior, es requerido realizar los siguientes pasos:

- Cargar imagenes que se encuentran local al cluster de minikube, ejecutar comandos en consola:
    - eval $(minikube docker-env) (no es requerido ejecutar el comando si ya fue ejecutado anteriormente si no se ha cerrado la consola)
    - minikube cache add nombre-imagen-nginx (utilizar la que fue creada en el reto 2)
    - minikube cache reload
    - minikube cache list
- Crear namespace, ejecutar comando:
    - kubectl create namespace clmhelm
- Instalar helm chart ejecutando el siguiente comando en la consola (debe estar posicionado dentro de la carpeta Reto 5):
    - helm install clm-1.0.0 ./helmchart/ --namespace clmhelm --no-hooks
- Ejecute el siguiente comando para acceder a su aplicación a través de un navegador:
    - minikube service nginx --url -n clmhelm (seleccionar url que se enccuentra a lado del puerto 443, ejemplo: "https/443    | http://192.168.49.2:30783"
- Considerando el punto anterior, copiar la url mencionada en la barra de navegación del navegador para acceder a su aplicación, ejemplo: "https://192.168.49.2:30783", es importante resaltar que es necesario indicar https ya que se redirecciona todo el tráfico 80 --> 443.
- Luego de ingresar la url y presionar tecla enter, se muestra que es un sitio no seguro, hacer clic en enlace de continuar, posterior a ello, se solicitará usuario y contraseña, para ambos casos indicar: clm
